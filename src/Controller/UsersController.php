<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 7/20/2017
 * Time: 4:16 PM
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Symfony\Component\Config\Definition\Exception\Exception;


class UsersController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		// Allow users to register and logout.
		// You should not add the "login" action to allow list. Doing so would
		// cause problems with normal functioning of AuthComponent.
		$this->Auth->allow(['add', 'logout', 'login', 'userConfirmation']);
	}

	public function index()
	{
		$this->set('users', $this->Users->find('all'));
	}

	public function view($id)
	{
		$user = $this->Users->get($id);
		$this->set(compact('user'));
	}

	public function add()
	{
		$this->loadModel('Users');
		$user = $this->Users->newEntity();

		if ($this->request->is('post')) {
			try {
				$mail = $this->request->data["email"];
				$exist = $this->Users->get_exist_user($mail);
				if(count($exist) > 0)
					throw new Exception("Email address already register");
				if( strlen($this->request->data["password"]) < 6)
					throw new Exception("Password at least 6 character");

				$user = $this->Users->patchEntity($user, $this->request->getData());
				$result = $this->Users->save($user);
				if ($result) {
					// Retrieve user from DB
					//$authUser = $this->Users->get($result->id)->toArray();
					// Log user in using Auth
					//$this->Auth->setUser($authUser);
					// Redirect user
					//$this->redirect(['controller' => 'Home', 'action' => 'index']);

					$url = Router::url('/', true) . "users/userConfirmation/" . base64_encode($result->id);
					$fromLabel = (Configure::read('quiz.envrionment') === 'PRODUCTION') ? "quiz.mail_from" : "quiz.mail_from_dev";
					$from = Configure::read($fromLabel);
					$subject = __('Email verification');
					$replyTo = Configure::read('quiz.replay_to');
					$subject = mb_convert_encoding($subject, 'UTF-8');
					$send_email = new Email('default');
					$send_email->setFrom([$from => 'QUIZ SUPPORT'])
						->setSender($from, 'QUIZ SUPPORT')
						->setViewVars(['url' => $url])
						->setTemplate('auth_confirmation')
						->setEmailFormat('both')
						->setTo($mail)
						->setReplyTo($replyTo)
						->setSubject($subject)
						->send();

					$this->Flash->success(__('Please check you mail for verification.'));
					$this->redirect(['action' => 'add']);
				} else {
					$this->Flash->error(__('Unable to add the user.'));
				}
			} catch (Exception $e) {
				//$this->setFlashAlert($e->getMessage());
				$this->Flash->error($e->getMessage());
			}

		}
		$this->set('title','Register | OC-Quiz');
		$this->set('user', $user);
	}
	public function login()
	{
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user && $user['active']) {
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid email or password, try again'));
		}
		$this->set('title','Login | OC-Quiz');
	}
	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}
	/*Make active the user after register
	*@author<Utpal Biswas>
	*/
	public function userConfirmation($user_id){
		$this->loadModel('Users');
		try {
			if (!$this->request->is('get')) throw new Exception("Bad request");
			if (empty($user_id)) throw new Exception("invalid paramiter");

			$user = $this->Users->find('all', [
				'conditions' => ['id' => base64_decode($user_id), 'active' => 0]
			])->first();
			if (empty($user)) throw new Exception("User Not registered");
			$user = $user->toArray();
			$user_data = $this->Users->get($user['id']);
			$user_data->active = 1;
			$user_data->updated = date('Y-m-d H:i:s');
			$user = $this->Users->save($user_data);
			$user_id = $user['id'];
			$this->Cookie->write('User.id', $user_id);
			$this->redirect(
				['controller' => 'Users', 'action' => 'login']
			);
		} catch (Exception $e) {
			$this->Flash->error($e->getMessage());
			die();
		}
	}


}
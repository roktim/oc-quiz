<?php
/**
 * Created by PhpStorm.
 * User: sahid
 * Date: 20/07/17
 * Time: 1:59 PM
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');/*This line loads the required RequestHandler Component*/
        $this->loadComponent('Paginator');
    }


    /*
     */
    public function index()
    {
        $this->set('title','Home | OC-Quiz');
    }

}


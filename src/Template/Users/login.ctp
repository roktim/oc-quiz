<section id="login">
	<div class="oc-container">
		<div class="oc-row login">
				<?= $this->Form->create('Users',['method'=>'post', 'class'=>'login_form', 'url' => ['action' => 'login']]); ?>
				<h1>Login</h1>
			<div class="message"><?= $this->Flash->render() ?></div>
			<!-- /.message -->
				<p>ユーザー名</p>
				<?= $this->Form->control('email',['label'=>false]) ?> <br>

				<p>パスワード</p>
				<?= $this->Form->control('password',['label'=>false]) ?>
				<?= $this->Form->button('ログイン',['class'=>'btn login_btn']); ?>
				<div class="register_link">
					<?php echo $this->Html->link(
						'Register here',
						'/users/add',
						['class' => 'register_link']
					); ?>
				</div>
				<!-- /.register_link -->
				<?= $this->Form->end() ?>
		</div>
	</div>
</section>
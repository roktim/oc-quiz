<section id="login">
	<div class="oc-container">
		<div class="oc-row login">
			<?= $this->Form->create('Users',['method'=>'post', 'class'=>'login_form', 'url' => ['action' => 'add']]); ?>
			<h1>Register</h1>
			<div class="message"><?= $this->Flash->render() ?></div>
			<!-- /.message -->
			<p>ユーザー名</p>
			<?= $this->Form->control('name',['label'=>false,'required'=>true]) ?> <br>

			<p>Email</p>
			<?= $this->Form->control('email',['label'=>false,'required'=>true]) ?><br>

			<p>パスワード</p>
			<?= $this->Form->control('password',['label'=>false,'required'=>true]) ?>

			<?= $this->Form->button('ログイン',['class'=>'btn login_btn']); ?>
			<div class="register_link">
				<?php echo $this->Html->link(
					'Login here',
					'/users/login',
					['class' => 'register_link']
				); ?>
			</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</section>
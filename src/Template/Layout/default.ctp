<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>
        <?= $title; ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('normalize.css') ?>
    <?= $this->Html->css('main.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->meta(''); ?>
    <?= $this->Html->meta(''); ?>

</head>
<body>
<div class="container">
    <?= $this->Flash->render() ?>
    <header id="header">
        <div class="oc-container">
            <div class="header_top">
                <div class="oc-row header">
                    <div class="logo">
                        <?php echo $this->Html->image('N3Logo.png');?>
                    </div>
                    <div class="title">
                        <h1>Practice tool for study IT Japanese</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="main_content">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
        <div class="oc-container">
            <div class="footer-bg">
                <p class="footer-txt">Copyright (C) The Japan Foundation / Japan Educational Exchanges and Services</p>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <?= $this->Html->script('plugins.js'); ?>
    <?= $this->Html->script('main.js'); ?>
    <?= $this->fetch('script') ?>
</body>
</html>
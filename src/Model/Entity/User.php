<?php
/**
 * Created by PhpStorm.
 * User: Utpal
 * Date: 7/20/2017
 * Time: 4:19 PM
 */


namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\Validation\Validator;

class User extends Entity
{

	// Make all fields mass assignable except for primary key field "id".
	protected $_accessible = [
		'*' => true,
		'id' => false
	];

	// ...

	protected function _setPassword($password)
	{
		if (strlen($password) > 0) {
			return (new DefaultPasswordHasher)->hash($password);
		}
	}

	
}
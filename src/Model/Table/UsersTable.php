<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class UsersTable extends Table {

	public $alias = "Users";

	public function initialize(array $config)
	{
		$this->table('users');
	}
	public function get_exist_user($username){
		$query = $this->find()
			->where(['Users.email' => $username]);
		return  $query->first();
	}

	public function validationDefault(Validator $validator) {
		return $validator
			->notEmpty('username', 'A username is required')
			->notEmpty('password', 'A password is required');
	}

}